```
irb -r ./hippie.rb
```

```
100.times do 
	puts Hippie.speak
end
```

Roadmap: 

More sentences
Inflections need (to be able to turn nouns into plural nouns)
Verb conjugation
Sentiment weighting? (i.e. have some "negative" sentence structures that can be paired with "negative" words)
capitalization / punctuation

