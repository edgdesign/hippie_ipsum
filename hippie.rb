require './hippie/sentence'
require './hippie/word'

require 'verbs'
require 'active_support/inflector'
# gem: linguistics looks promising - can handle manipulation of nouns, adjectives, verbs, etc -- https://www.rubydoc.info/gems/linguistics/2.1.0/toplevel

	
module Hippie

	class << self
		def speak
			madlib.inject('') do |sentence, el|

				sentence << case el
				when String
					el
				when Symbol
					send el
				when Hash
					send el[:type], el
				end

				sentence
			end.capitalize
		end

		def madlib
			Sentence::SENTENCES.sample
		end

		def noun config = {}
			word = Word::NOUNS.sample
			word = word.pluralize if config[:plural]
			word = word.prepend(word.match?(/^[aeiou]/i) ? 'an ' : 'a ') if config[:article]
			word
		end

		def adjective config = {}
			Word::ADJECTIVES.sample
		end

		def verb config = {}
			word = Word::VERBS.sample
			word = word.verb.conjugate config
			word
		end
	end
end