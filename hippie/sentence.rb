module Hippie
	module Sentence
		SENTENCES = [
			['My hope is these ', {:type => :noun, :plural => true}, ' are a ', :adjective, ' part of your ', :adjective, ' tool box, easily accessible for you to use.' ],
			['Beautiful ', {:type => :noun, :plural => true}, ' to reprogram your ', :noun, 'are included in my next 30 day ', :adjective, ' ', :noun, '.'],
			['This time ', {:type => :verb, :tense => :future}, ' the opportunity of a ', :noun, '.'],
			['Be present with the ', :adjective, ' ', :adjective, ' ', :noun, '.'],
			['We can ', :verb, ' the ', :noun, '.'],
			['If we can ', {:type => :verb, :tense => :present, :plurality => :plural}, ' the ', {:type => :noun, :plural => true}, ', ', {:type => :verb, :tense => :future, :person => :first, :plurality => :plural, :subject => true}, ' the ', :adjective, ' ', :noun, '.'],
			['Our ancestors ', {:type => :verb, :tense => :past, :plurality => :plural}, ' the ', :noun, '.'],
			[:verb, ' your ', :adjective, ' ', :noun, '.'],
			[{:type => :noun, :article => true}, ' is aware of their own ', {:type => :noun, :article => false}, ' and puts that power into action.'],
			['You are more ', :adjective, ' than any ', {:type => :noun}, ' money can buy.'],
			['Work with the ', {:type => :noun} ,', not against it.'],
			[{:type => :noun}, ' is more important than ', {:type => :noun}, '.'],
			['If you want a ', :adjective, ' life, surround yourself with ', :adjective , ' people.'],
			['When you change your ', {:type => :noun, :plural => true}, ', you open yourself up to a whole new ', {:type => :noun}, '.']

			# [<verb-ing> is a <adjective> <nou]
			# [<adjective> <adjective> <noun-s> are <verb-ing> <noun-s>]
		]
	end
end