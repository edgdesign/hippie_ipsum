# TODOs

- adjectives should be able to handle articles (a / an) too.
- noun :article option seems to bleed over -- if I define a sentence with two nouns, the first of which has :article => true, the second will *sometimes* have an article added to it (if the second noun's :article option is omitted), behavior doesn't seem consistent though. 